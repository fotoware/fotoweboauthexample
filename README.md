# FotoWeb 2.0 OAuth Example Application

This application is for demonstrating and testing OAuth 2.0 support in FotoWeb.

The application as well as usage instructions are hosted publicly at [https://fwoauthdemo.fotoware.cloud/](https://fwoauthdemo.fotoware.cloud/).

This example application IS NOT a framework for a production-ready web application.
Only the components related directly to OAuth 2.0 can be reused largely unmodified
(after removing the code for different types of applications than the type of application one is building).
The application has been deliberately kept simple (for example, it does not use a database for persisting sessions)
to make it easy to build, run, and deploy in any development environment.

See LICENSE.txt for licensing and other legal information.

## Building

This code can be built and run in Microsoft Visual Studio 2015 or later.

## Example Code

Example code for real-world OAuth applications can be found in the following files:

### For a web or native application

1. In `WebAppController.cs`, the method `Login` builds the authorization request.
2. In `OAuth2Controller.cs`, the method `CallbackWeb` handles the redirection callback.
3. In `WebAppController.cs`, the method `Refresh` requests a new access token using a refresh token.

### For single page JavaScript application

See the file `SinglePageApp.js`.

### For single page JavaScript application or CMS integration using OAuth

See the file `SinglePageApp.js` and `Index.cshtml` in either `SinglePageApp` or `SelectionWidgetApp`.

### Using Widgets

See the file `Widget.js` and `Index.cshtml` in either `SinglePageApp` or `SelectionWidgetApp`.
