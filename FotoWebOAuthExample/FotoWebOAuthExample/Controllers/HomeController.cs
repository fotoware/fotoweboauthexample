﻿using FotoWebOAuthExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FotoWebOAuthExample.Controllers
{
    /// <summary>
    /// Home page controller.
    /// This is a demo application that is hosted publicly, and there is no configuration interface for administrators.
    /// Therefore, the application initially asks for all the OAuth 2.0 configuration parameters, which can be obtained when registering an application in FotoWeb.
    /// In a real world application, there would either be a separate configuration interface for administrators only,
    /// or the parameters would be stored in a database, configuration file, application settings (web.config) or hard-coded in the source code.
    /// In this application, the parameters either have to be entered in the form, or they can be passed in the query string.
    /// </summary>
    public class HomeController : Controller
    {
        // GET home page
        public ActionResult Index(ApplicationDetails appDetails)
        {
            ModelState.Clear();

            // The application details can be sent in the request URL to skip the form.
            // This makes it easy to link to the application for repeated tests or demonstrations.
            // If application details were not given, or are incomplete, show the form.
            if (appDetails == null || !appDetails.Run || !appDetails.IsValid())
            {
                appDetails.Run = true;
                return View(appDetails);
            }

            // The redirection URI is normally a constant URL of the callback request handler.
            // Here, it depends on the type of application, so we compute it after it has been provided by the user.
            string action = null;
            if (appDetails.Type == ApplicationType.Web)
                action = "CallbackWeb";
            else if (appDetails.Type == ApplicationType.Native)
                action = "CallbackNative";
            else if (appDetails.Type == ApplicationType.WebSinglePage)
                action = "CallbackJS";
            else if (appDetails.Type == ApplicationType.SelectionWidget)
                action = "CallbackSelectionWidgetWithBackEnd";
            else if (appDetails.Type == ApplicationType.SelectionWidgetSinglePage)
                action = "CallbackSelectionWidget";
            if (action != null)
            {
                appDetails.RedirectUri = new UriBuilder(Request.Url.AbsoluteUri)
                {
                    Path = Url.Action(action, "OAuth2"),
                    Query = null
                }.Uri.ToString();
            }

            // Run the selected type of application and pass the configuration to it.
            if (appDetails.Type == ApplicationType.Web || appDetails.Type == ApplicationType.Native || appDetails.Type == ApplicationType.SelectionWidget)
            {
                // Web apps and "simulated" native apps are very similar, so the same controller is used to implement both.
                return RedirectToAction("Index", "WebApp", appDetails);
            }
            else if(appDetails.Type == ApplicationType.WebSinglePage || appDetails.Type == ApplicationType.SelectionWidgetSinglePage)
            {
                // Single page apps are different and mostly implemented in JavaScript, so a different controller is used.
                return RedirectToAction("Index", "SinglePageApp", appDetails);
            }
            else
            {
                // A selection widget (CMS integration) app does not itself request authorization from FotoWeb.
                // It only embeds the FotoWeb selection widget in an IFRAME.
                return RedirectToAction("Index", "SelectionWidgetApp", appDetails);
            }
        }
    }
}
