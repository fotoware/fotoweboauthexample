﻿using FotoWebOAuthExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FotoWebOAuthExample.Controllers
{
    /// <summary>
    /// An application which embeds the selection widget in an IFRAME.
    /// This is a typical "CMS integration" app. In this example, it consists of static HTML and JavaScript only.
    /// </summary>
    public class SelectionWidgetAppController : Controller
    {
        // GET: SelectionWidgetApp
        public ActionResult Index(ApplicationDetails appDetails)
        {
            return View(appDetails);
        }
    }
}