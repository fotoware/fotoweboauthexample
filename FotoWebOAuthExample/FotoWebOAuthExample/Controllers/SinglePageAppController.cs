﻿using FotoWebOAuthExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FotoWebOAuthExample.Controllers
{
    /// <summary>
    /// Example FotoWeb OAuth 2.0 single page application.
    /// This controller is the main entry point of a single page JavaScript application without a back-end.
    ///
    /// For this reason, all it does is show a "static" HTML page, which includes static JavaScript.
    ///
    /// Note that, since the application configuration (ApplicationDetails, obtained from HomeController) is dynamic,
    /// it has to be passed by the query string and to the application template.
    /// In a real world application, there would either be a separate configuration interface for administrators only,
    /// or the parameters would be stored in a database, configuration file, application settings (web.config) or hard-coded in the source code.
    /// </summary>
    public class SinglePageAppController : Controller
    {
        // GET: SinglePageApp
        public ActionResult Index(ApplicationDetails appDetails)
        {
            return View(appDetails);
        }
    }
}