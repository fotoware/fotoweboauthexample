﻿using FotoWebOAuthExample.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FotoWebOAuthExample.Controllers
{
    /// <summary>
    /// Example FotoWeb OAuth 2.0 web or native application.
    /// This controller is the main entry point of a web application with a back-end.
    /// It is also used to "simulate" a native application, which uses a very similar OAuth 2.0 protocol flow.
    ///
    /// This controller has two actions:
    /// One is the entry point, which sends the authorization request to FotoWeb.
    /// The other one is the start page of the application after completed authorization.
    /// 
    /// Note that, since the application configuration (ApplicationDetails, obtained from HomeController) is dynamic,
    /// it has to be passed by the query string.
    /// In a real world application, there would either be a separate configuration interface for administrators only,
    /// or the parameters would be stored in a database, configuration file, application settings (web.config) or hard-coded in the source code.
    /// </summary>
    public class WebAppController : Controller
    {
        /// <summary>
        /// Home page of web or native application.
        /// Shows the "login view" with the "Log in with FotoWeb" button.
        /// </summary>
        /// <param name="appDetails"></param>
        /// <returns></returns>
        public ActionResult Index(ApplicationDetails appDetails)
        {
            // If application details are missing or incomplete or application type is not correct, redirect to application details form.
            if (!appDetails.IsValid())
                return RedirectToAction("Index", "Home");
            if (appDetails.Type != ApplicationType.Web && appDetails.Type != ApplicationType.Native && appDetails.Type != ApplicationType.SelectionWidget)
                return RedirectToAction("Index", "Home");

            return View();
        }

        /// <summary>
        /// "Log in" action of web or native application.
        /// Called when user clicks the "Log in with FotoWeb" button.
        /// This function starts the OAuth authorization process for web or native applications.
        /// </summary>
        /// <param name="appDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(ApplicationDetails appDetails)
        {
            if (appDetails == null || !appDetails.IsValid())
            {
                return new HttpStatusCodeResult(400, "Missing or invalid application configuration");
            }

            // Create a new authorization state.
            // Authorization is state-full, because we need the PKCE code verifier when sending the token request.
            // Note that a multi-user app (most web apps) must be able to handle multiple authorizations at once,
            // which is why we use a "database" of states here (you may think of each state as a distinct user session).
            // A native application is typically only used by one user at a time, so a single state is OK.
            var state = State.Create(appDetails);

            // Build the URL of the authorization endpoint.
            var authUri = new UriBuilder(appDetails.TenantUrl);
            authUri.Path = "/fotoweb/oauth2/authorize";

            // Build the parameters of the authorization request as URL query string.
            // Web apps and native webs use authorization code flow and ask FotoWeb to send an authorization code to the redirection endpoint.
            // The authorization code is later used to request an access token.
            var authQuery = System.Web.HttpUtility.ParseQueryString(string.Empty);
            authQuery["response_type"] = "code";
            authQuery["client_id"] = appDetails.GetClientId();
            authQuery["state"] = state.Id;

            // ONLY FOR NATIVE APPLICATIONS
            // The redirection callback is handled differently in web apps and native apps.
            // Therefore, we use different redirection URLs. Both are handled by OAuth2Controller, but by different actions.
            if (appDetails.Type == ApplicationType.Native)
            {
                // For native apps only:
                // Send PKCE code challenge in authorization request, which FotoWeb later uses to validate the code verifier sent in the token request.
                // This is necessary, because the application cannot authenticate itself.
                // PKCE prevents a malicious application from intercepting and using the authorization code.
                authQuery["code_challenge_method"] = "S256";
                authQuery["code_challenge"] = state.CodeChallenge;
            }

            // The redirect URI needs to be sent in the token request if and only if it was also sent in the authorization request.
            // This is only necessary if an application has multiple redirect URIs.
            // Otherwise, the redirect_uri parameter can be omitted.
            // In this application, the redirection URI is always of the form /OAuth2/CallbackXXX where XXX depends on application type.
            // Each application type is a separately registered application, which only needs one single redirection URI.
            // However, we send the redirect_uri parameter anyway to demonstrate how it is done.
            // The redirection endpoint is implemented in OAuth2Controller.
            authQuery["redirect_uri"] = appDetails.RedirectUri;

            // Redirect the browser to the authorization endpoint of FotoWeb.
            // FotoWeb will ask the user to authenticate (using the regular login screen) or use an existing user session.
            // FotoWeb may ask the user for consent to authorize the application.
            // Then, FotoWeb will redirect to the redirection endpoint (see OAuth2Controller).
            authUri.Query = authQuery.ToString();
            return Redirect(authUri.Uri.ToString());
        }

        /// <summary>
        /// Main view of web or native application.
        /// Shows the main view of the application after successful login.
        /// If no session cookie is set, or the session is not valid, then redirect to the login page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Main()
        {
            var session = _GetSession();
            if (session == null) return RedirectToAction("Index");

            return View(session);
        }

        /// <summary>
        /// Action for testing refresh tokens.
        /// This is not part of a real world application. It is for testing and demonstrating OAuth refresh tokens only.
        /// This is called when the user clicks the "Test Refresh Token" button.
        /// </summary>
        /// <returns></returns>
        public ActionResult Refresh()
        {
            var session = _GetSession();
            if (session == null) return RedirectToAction("Index");

            // Build the URL of the token endpoint.
            var uri = new UriBuilder(session.ApplicationDetails.TenantUrl);
            uri.Path = "/fotoweb/oauth2/token";

            // Build the token request.
            // This is a POST request containing URL-encoded form data.
            // The response is in JSON format.
            var request = (HttpWebRequest)WebRequest.Create(uri.Uri.ToString());
            request.AllowAutoRedirect = true;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            // Build the parameters of the token request as URL-encoded form data.
            // These will be sent as the request body.
            var query = System.Web.HttpUtility.ParseQueryString(string.Empty);
            query["grant_type"] = "refresh_token";
            query["refresh_token"] = session.RefreshToken;
            query["client_id"] = session.ApplicationDetails.GetClientId();

            // Only for web apps with back-end: Send client secret to authenticate the application.
            // A web app can store the client secret securely on a server that is not publicly accessible.
            // Once the application is authenticated, FotoWeb can trust it and process the token request.
            if (session.ApplicationDetails.Type == ApplicationType.Web || session.ApplicationDetails.Type == ApplicationType.SelectionWidget)
                query["client_secret"] = session.ApplicationDetails.GetClientSecret();

            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(query.ToString());
            }

            var tokenResponse = (HttpWebResponse)request.GetResponse();

            // Parse the response to get the new access token and new refresh token
            using (var reader = new StreamReader(tokenResponse.GetResponseStream()))
            {
                var json = JsonConvert.DeserializeObject<Dictionary<string, string>>(reader.ReadToEnd());

                session.AccessToken = json["access_token"];
                session.RefreshToken = json["refresh_token"];
            }

            // Redirect to the main page of the web or simulated native application.
            return RedirectToAction("Main");
        }

        /// <summary>
        /// Get session state using session cookie
        /// </summary>
        /// <returns>Session state</returns>
        private State _GetSession()
        {
            // Get the authorization state (and thus the access token) from the session cookie that was set in the callback handler (Oauth2Controller).
            var sessionCookie = Request.Cookies.Get("Session");
            if (sessionCookie == null)
            {
                return null;
            }
            return State.Get(sessionCookie.Value);
        }
    }
}