﻿using FotoWebOAuthExample.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FotoWebOAuthExample.Controllers
{
    /// <summary>
    /// OAuth2 redirection endpoints.
    /// This controller has separate endpoints for all types of example applications.
    /// In a real application, you need one of these, depending on the type of your application.
    /// </summary>
    public class OAuth2Controller : Controller
    {
        /// <summary>
        /// Redirection endpoint for web applications
        /// </summary>
        /// <param name="code">Authorization code</param>
        /// <param name="state">State ("session ID")</param>
        /// <param name="error">Error code</param>
        /// <param name="error_description">Error description</param>
        /// <param name="error_uri">Error information URI</param>
        public ActionResult CallbackWeb(string code = "", string state = "", string error = "", string error_description = "", string error_uri = "")
        {
            if (!String.IsNullOrEmpty(code) && !String.IsNullOrEmpty(state))
                return _HandleAuthorizationCode(code, state);
            else if (!String.IsNullOrEmpty(error))
                return _HandleError(error, error_description, error_uri);
            else
                return _HandleError("", "Invalid response from server", "");
        }

        /// <summary>
        /// Redirection endpoint for CMS integration application with back-end
        /// </summary>
        /// <param name="code">Authorization code</param>
        /// <param name="state">State ("session ID")</param>
        /// <param name="error">Error code</param>
        /// <param name="error_description">Error description</param>
        /// <param name="error_uri">Error information URI</param>
        public ActionResult CallbackSelectionWidgetWithBackEnd(string code = "", string state = "", string error = "", string error_description = "", string error_uri = "")
        {
            if (!String.IsNullOrEmpty(code) && !String.IsNullOrEmpty(state))
                return _HandleAuthorizationCode(code, state);
            else if (!String.IsNullOrEmpty(error))
                return _HandleError(error, error_description, error_uri);
            else
                return _HandleError("", "Invalid response from server", "");
        }

        /// <summary>
        /// Redirection endpoint for native applications
        /// </summary>
        /// <param name="code">Authorization code</param>
        /// <param name="state">State ("session ID")</param>
        /// <param name="error">Error code</param>
        /// <param name="error_description">Error description</param>
        /// <param name="error_uri">Error information URI</param>
        public ActionResult CallbackNative(string code = "", string state = "", string error = "", string error_description = "", string error_uri = "")
        {
            if (!String.IsNullOrEmpty(code) && !String.IsNullOrEmpty(state))
                return _HandleAuthorizationCode(code, state);
            else if (!String.IsNullOrEmpty(error))
                return _HandleError(error, error_description, error_uri);
            else
                return _HandleError("", "Invalid response from server", "");
        }

        /// <summary>
        /// Redirection endpoint for single-page JavaScript applications
        /// </summary>
        public ActionResult CallbackJS()
        {
            // Redirect to single page HTML + JavaScript application.
            // All the parameters are contained in the fragment part of the URL and never seen by the server.
            // They are handled by JavaScript code in SinglePageApp.js
            return RedirectToAction("Index", "SinglePageApp");
        }

        /// <summary>
        /// Redirection endpoint for CMS integration application without back-end
        /// </summary>
        public ActionResult CallbackSelectionWidget()
        {
            // Redirect to single page HTML + JavaScript application.
            // All the parameters are contained in the fragment part of the URL and never seen by the server.
            // They are handled by JavaScript code in SinglePageApp.js
            return RedirectToAction("Index", "SinglePageApp");
        }

        /// <summary>
        /// Common implementation for handling native and web callbacks using authorization codes.
        /// Validates the authorization code and generates an access token.
        /// </summary>
        /// <param name="code">Authorization code</param>
        /// <param name="state">State ("Session ID")</param>
        private ActionResult _HandleAuthorizationCode(string code, string state)
        {
            // Use the state parameter to get the state of the current authorization request.
            // Authorization is state-full, because we need the PKCE code verifier when sending the token request.
            // This also prevents cross-site request forgery (CSRF) attacks against the application,
            // because the state string received from the user agent has to be equal to the state string sent in the authorization request.
            // Note that a multi-user app (most web apps) must be able to handle multiple authorizations at once,
            // which is why we use a "database" of states here (you may think of each state as a distinct user session).
            // A native application is typically only used by one user at a time, so a single state is OK.
            var session = State.Get(state);
            if (session == null) return RedirectToAction("Index", "WebApp");  // Invalid state, redirect to log-in page

            try
            {
                // Build the URL of the token endpoint.
                var uri = new UriBuilder(session.ApplicationDetails.TenantUrl);
                uri.Path = "/fotoweb/oauth2/token";

                // Build the token request.
                // This is a POST request containing URL-encoded form data.
                // The response is in JSON format.
                var request = (HttpWebRequest)WebRequest.Create(uri.Uri.ToString());
                request.AllowAutoRedirect = true;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Accept = "application/json";

                // Build the parameters of the token request as URL-encoded form data.
                // These will be sent as the request body.
                var query = System.Web.HttpUtility.ParseQueryString(string.Empty);
                query["grant_type"] = "authorization_code";
                query["code"] = code;
                query["client_id"] = session.ApplicationDetails.GetClientId();

                // Only for native apps: Send PKCE code verifier in token request to prove possession of authorization code.
                // This is necessary, because the application cannot authenticate itself.
                // PKCE prevents a malicious application from intercepting and using the authorization code.
                if (session.ApplicationDetails.Type == ApplicationType.Native)
                    query["code_verifier"] = session.CodeVerifier;

                // Only for web apps with back-end: Send client secret to authenticate the application.
                // A web app can store the client secret securely on a server that is not publicly accessible.
                // Once the application is authenticated, FotoWeb can trust it and process the token request.
                if (session.ApplicationDetails.Type == ApplicationType.Web || session.ApplicationDetails.Type == ApplicationType.SelectionWidget)
                    query["client_secret"] = session.ApplicationDetails.GetClientSecret();

                // The redirect URI needs to be sent in the token request if and only if it was also sent in the authorization request.
                // This is only necessary if an application has multiple redirect URIs.
                // Otherwise, the redirect_uri parameter can be omitted.
                query["redirect_uri"] = new UriBuilder(Request.Url.AbsoluteUri) { Query = null }.Uri.ToString();

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(query.ToString());
                }

                var tokenResponse = (HttpWebResponse)request.GetResponse();

                // Parse the response to get the access token and refresh token
                using (var reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    var json = JsonConvert.DeserializeObject<Dictionary<string, string>>(reader.ReadToEnd());

                    session.AccessToken = json["access_token"];
                    session.RefreshToken = json["refresh_token"];
                }
            }
            catch (WebException e)
            {
                // Invoke the error view.
                // This is normally used for OAuth errors from the server, but we reuse it here.
                // A typical reason for the token request failing is that the FotoWeb site is not accessible from the client app
                // (For example, if the client app is hosted on the public internet and the FotoWeb site isn't)
                return _HandleError("token_request_failed", $"The token request has failed: {e.Message}", "");
            }

            // ONLY FOR API APPLICATIONS
            // Request the API descriptor and add information about the user to the session.
            // This is optional, but typically the first thing an API client would do to get information about the API and user.
            // Widget applications cannot make API calls, so they cannot get the API descriptor.
            if (!session.ApplicationDetails.IsSelectionWidgetApp)
            {
                session.UserDisplayName = _GetAPIDescriptor(session);
            }

            // Create a user session.
            // This has nothing to do with OAuth. It is up to the application how to manage user sessions.
            // In this demo application, we simply use a session cookie with the same ID as the authorization state.
            // In this way, we never expose the access token to the user agent.
            // The cookie is protected from access by JavaScript running in the browser, so it cannot be "stolen" by XSS attacks.
            Response.Cookies["Session"].Value = session.Id;
            Response.Cookies["Session"].Secure = Request.IsSecureConnection;  // Send cookie only if HTTPS is used (unless authorization was done on HTTP on a non-production system)
            Response.Cookies["Session"].HttpOnly = true;  // Prevent JavaScript from accessing the cookie

            // Redirect to the main page of the web or simulated native application.
            return RedirectToAction("Main", "WebApp", session.ApplicationDetails);
        }

        /// <summary>
        /// Handle OAuth 2.0 error response
        /// </summary>
        /// <param name="error">Error identifier</param>
        /// <param name="errorDescription">Human-readable and localized error string provided by FotoWeb</param>
        /// <param name="errorURI">Optional error help URI provided by FotoWeb</param>
        private ActionResult _HandleError(string error, string errorDescription, string errorURI)
        {
            // Show error page
            return View("Error", new OAuth2Error { Id = error, Description = errorDescription, Uri = errorURI });

            // If an application does not handle errors, it can also simply redirect back to its own home page.
            // return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Get the API descriptor from the FotoWeb API entry point.
        /// This method demonstrates how to use the FotoWeb API with an OAuth access token.
        /// It simply returns the full display name of the user (e.g., "John Smith").
        /// </summary>
        /// <param name="session">Current authentication state (contains access token)</param>
        /// <returns>Information from API descriptor</returns>
        private string _GetAPIDescriptor(State session)
        {
            var uri = new UriBuilder(session.ApplicationDetails.TenantUrl);
            uri.Path = "/fotoweb/me";

            // Build the request.
            // See https://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/01_The_FotoWeb_RESTful_API/04_API_Entry_Points
            var request = (HttpWebRequest)WebRequest.Create(uri.Uri.ToString());
            request.AllowAutoRedirect = true;
            request.Method = "GET";
            request.Accept = "application/vnd.fotoware.full-api-descriptor+json";

            // Set access token header.
            // This is required for all API requests.
            request.Headers["Authorization"] = $"Bearer {session.AccessToken}";

            var response = (HttpWebResponse)request.GetResponse();

            // Parse the response to get the access token and refresh token
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                var definition = new { user = new { fullName = "" } };
                var json = JsonConvert.DeserializeAnonymousType(reader.ReadToEnd(), definition);
                return json.user.fullName;
            }
        }
    }
}