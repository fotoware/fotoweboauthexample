﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FotoWebOAuthExample.Models
{
    /// <summary>
    /// OAuth 2.0 error information
    /// </summary>
    public class OAuth2Error
    {
        /// <summary>
        /// Error identifier.
        /// Can be used to programmatically distinguish between types of OAuth 2.0 errors and handle them accordingly.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Human-readable and localized error string provided by FotoWeb.
        /// May be useful for developers and administrators, but not particularly friendly to users.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Error URI.
        /// An optional error URI provided by FotoWeb.
        /// If given, it points to a help page with more information about the error.
        /// </summary>
        public string Uri { get; set; }
    }
}