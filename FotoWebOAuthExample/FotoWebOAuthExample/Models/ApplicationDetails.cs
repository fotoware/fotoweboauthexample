﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FotoWebOAuthExample.Models
{
    /// <summary>
    /// Type of FotoWeb OAuth 2.0 application
    /// </summary>
    public enum ApplicationType
    {
        /// <summary>
        /// A web application or web API with a server component
        /// </summary>
        [Display(Name = "Web application or API", Description = "A web application or web API with a server component")]
        Web,

        /// <summary>
        /// A web application consisting of only static HTML and JavaScript and no server component
        /// </summary>
        [Display(Name = "Web application (no back-end)", Description = "A web application consisting of only static HTML and JavaScript and no server component")]
        WebSinglePage,

        /// <summary>
        /// A native desktop or mobile application (simulated as a web application)
        /// </summary>
        [Display(Name = "Native application", Description = "A native desktop or mobile application (simulated as a web application)")]
        Native,

        /// <summary>
        /// A web application with a back-end using only FotoWeb widgets and not using the FotoWeb API
        /// </summary>
        [Display(Name = "CMS integration", Description = "A web application with a back-end using only FotoWeb widgets and not using the FotoWeb API")]
        SelectionWidget,

        /// <summary>
        /// A web application using only FotoWeb widgets and consisting of only static HTML and JavaScript and no server component
        /// </summary>
        [Display(Name = "CMS integration (no back-end)", Description = "A web application using only FotoWeb widgets and consisting of only static HTML and JavaScript and no server component")]
        SelectionWidgetSinglePage,

        /// <summary>
        /// A legacy web application using widgets, written for older versions of FotoWeb
        /// </summary>
        [Display(Name = "CMS integration (LEGACY)", Description = "A legacy web application using widgets, written for older versions of FotoWeb")]
        SelectionWidgetClassic
    };

    /// <summary>
    /// Details about a FotoWeb OAuth 2.0 application.
    /// Identifies the FotoWeb tenant and application, the type of application and contains credentials.
    /// </summary>
    public class ApplicationDetails
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ApplicationDetails()
        {
            Run = false;
            Type = ApplicationType.Web;
            TenantUrl = DemoServerTenantUrl;
        }

        /// <summary>
        /// Type of application (determines example UI)
        /// </summary>
        public ApplicationType Type { get; set; }

        /// <summary>
        /// FotoWeb Tenant URL (e.g., https://demo.fotoware.cloud)
        /// </summary>
        public string TenantUrl
        {
            get
            {
                return _tenantUrl;
            }

            set
            {
                // Parse, validate and normalize the URI.
                // We only need a scheme, host name, and port (if given).
                // We also do not want a final slash, as this leads to double-slashes and invalid URLs produced by JavaScript code
                try
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        var parsed = new UriBuilder(value);
                        var builder = new UriBuilder
                        {
                            Scheme = parsed.Scheme,
                            Host = parsed.Host,
                            Port = parsed.Port
                        };
                        _tenantUrl = builder.Uri.AbsoluteUri.TrimEnd(new char[] { '/' });
                        return;
                    }
                }
                catch (UriFormatException) {}

                _tenantUrl = null;
            }
        }

        /// <summary>
        /// Application client ID (GUID)
        /// </summary>
        public string ClientId
        {
            get
            {
                if (IsDemoServer())
                {
                    if (Type == ApplicationType.Web)
                        return DemoServerClientIdWeb;
                    else if (Type == ApplicationType.Native)
                        return DemoServerClientIdNative;
                    else if (Type == ApplicationType.WebSinglePage)
                        return DemoServerClientIdSinglePage;
                    else if (Type == ApplicationType.SelectionWidget)
                        return DemoServerClientIdWidget;
                    else if (Type == ApplicationType.SelectionWidgetSinglePage)
                        return DemoServerClientIdWidgetSinglePage;
                    else
                        return "";
                }
                else
                    return _clientId;
            }

            set
            {
                _clientId = value;
            }
        }

        /// <summary>
        /// Application client secret (for web apps only)
        /// </summary>
        public string ClientSecret { get; set; }

        /// <summary>
        /// Redirection URI of the application.
        /// This is normally a constant or configurable, but in this demo application (which is actually 3 different types of applications),
        /// it depends on the type of application, as we have a separate controller action for each application type.
        /// </summary>
        public string RedirectUri { get; set; }

        /// <summary>
        /// Login token for selection widget.
        /// This is for legacy applications using login tokens to authorize the selection widget.
        /// </summary>
        public string LoginToken { get; set; }

        /// <summary>
        /// Application is a CMS integration app which only uses widgets (no API)
        /// </summary>
        public bool IsSelectionWidgetApp { get { return Type == ApplicationType.SelectionWidget || Type == ApplicationType.SelectionWidgetSinglePage; } }

        /// <summary>
        /// Run the application if details are complete.
        /// If not given, the application details form is always shown.
        /// This prevents the application from being started right away when visiting the start page without parameters.
        /// It also allows "permalinking" of an application with a set of parameters.
        /// </summary>
        public bool Run { get; set; }

        /// <summary>Tenant URL of the demo server</summary>
        public static string DemoServerTenantUrl { get { return System.Configuration.ConfigurationManager.AppSettings["demoServerTenantURL"]; } }

        /// <summary>Client ID for web application registered on demo server</summary>
        public static string DemoServerClientIdWeb { get { return System.Configuration.ConfigurationManager.AppSettings["demoServerClientIdWeb"]; } }
        /// <summary>Client ID for native application registered on demo server</summary>
        public static string DemoServerClientIdNative { get { return System.Configuration.ConfigurationManager.AppSettings["demoServerClientIdNative"]; } }
        /// <summary>Client ID for single page application registered on demo server</summary>
        public static string DemoServerClientIdSinglePage { get { return System.Configuration.ConfigurationManager.AppSettings["demoServerClientIdSinglePage"]; } }
        /// <summary>Client ID for CMS integration application registered on demo server</summary>
        public static string DemoServerClientIdWidget { get { return System.Configuration.ConfigurationManager.AppSettings["demoServerClientIdWidget"]; } }
        /// <summary>Client ID for CMS integration application registered on demo server</summary>
        public static string DemoServerClientIdWidgetSinglePage { get { return System.Configuration.ConfigurationManager.AppSettings["demoServerClientIdWidgetSinglePage"]; } }

        /// <summary>Client secret for web application registered on demo server</summary>
        public static string DemoServerClientSecretWeb { get { return System.Configuration.ConfigurationManager.AppSettings["demoServerClientSecretWeb"]; } }
        /// <summary>Client secret for selection widget application with back-end registered on demo server</summary>
        public static string DemoServerClientSecretSelectionWidget { get { return System.Configuration.ConfigurationManager.AppSettings["demoServerClientSecretSelectionWidget"]; } }

        /// <summary>
        /// Application connects to the demo server (using default settings)
        /// </summary>
        bool IsDemoServer() { return !String.IsNullOrEmpty(DemoServerTenantUrl) && TenantUrl == DemoServerTenantUrl; }

        /// <summary>
        /// Get actual client ID.
        /// This is used internally, not as part of the view model, so the client ID is not unnecessarily exposed publicly.
        /// </summary>
        /// <returns>Client ID</returns>
        public string GetClientId()
        {
            if (IsDemoServer())
            {
                if (Type == ApplicationType.Web)
                    return DemoServerClientIdWeb;
                else if (Type == ApplicationType.Native)
                    return DemoServerClientIdNative;
                else if (Type == ApplicationType.WebSinglePage)
                    return DemoServerClientIdSinglePage;
                else if (Type == ApplicationType.SelectionWidget)
                    return DemoServerClientIdWidget;
                else if (Type == ApplicationType.SelectionWidgetSinglePage)
                    return DemoServerClientIdWidgetSinglePage;
                else
                    return "";
            }
            else
                return ClientId;
        }

        /// <summary>
        /// Get actual client secret.
        /// This is used internally, not as part of the view model, so the client secret is not exposed publicly.
        /// </summary>
        /// <returns>Client ID</returns>
        public string GetClientSecret()
        {
            if (IsDemoServer())
            {
                if (Type == ApplicationType.Web)
                    return DemoServerClientSecretWeb;
                else if (Type == ApplicationType.SelectionWidget)
                    return DemoServerClientSecretSelectionWidget;
                else
                    return "";
            }
            else
                return ClientSecret;
        }

        /// <summary>
        /// Check if application details are valid
        /// </summary>
        public bool IsValid()
        {
            // Tenant URL is always required, so the app can connect to FotoWeb.
            if (String.IsNullOrEmpty(TenantUrl))
                return false;

            // Client ID is required for all OAuth apps
            // (Classic CMS integrations do not use OAuth)
            if (Type == ApplicationType.Native || Type == ApplicationType.Web || Type == ApplicationType.WebSinglePage || Type == ApplicationType.SelectionWidget || Type == ApplicationType.SelectionWidgetSinglePage)
            {
                if (!IsDemoServer() && String.IsNullOrEmpty(ClientId))
                    return false;
            }

            // Client secret is required and allowed for web apps and for web apps only.
            if (Type == ApplicationType.Web || Type == ApplicationType.SelectionWidget)
            {
                if (!IsDemoServer() && String.IsNullOrEmpty(ClientSecret))
                    return false;
            }

            return true;
        }

        private string _clientId;
        private string _tenantUrl;
    }
}