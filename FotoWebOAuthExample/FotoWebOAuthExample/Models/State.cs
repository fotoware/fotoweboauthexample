﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace FotoWebOAuthExample.Models
{
    /// <summary>
    /// State of an authorization process.
    /// This is similar to a user session. Each state is identified by a string ID, which corresponds to the 'state' OAuth 2.0 parameter.
    /// Normally, state would be stored in a database, but in this simple example, we just use an in-memory map.
    /// </summary>
    public class State
    {
        /// <summary>
        /// State ID
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Details of application.
        /// </summary>
        public ApplicationDetails ApplicationDetails { get; }

        /// <summary>
        /// PKCE code verifier string.
        /// This is generated when building the authorization request URL and needs to be sent to the server when requesting an access token later.
        /// Therefore, the server has to store it and retrieve it in the OAuth 2.0 authorization callback.
        /// </summary>
        public string CodeVerifier { get; }

        /// <summary>
        /// PKCE code challenge string.
        /// This is a hash of the code verifier which is part of the OAuth 2.0 authorization URL for native and single-page applications.
        /// </summary>
        public string CodeChallenge { get { return _GetPKCEHash(CodeVerifier); } }

        /// <summary>
        /// Token for accessing the FotoWeb API.
        /// This is retrieved at the end of successful authorization and stored here.
        /// </summary>
        public string AccessToken { get; set; }
        
        /// <summary>
        /// Token for requesting a new access token when the current one has expired.
        /// This is retrieved at the end of successful authorization and stored here.
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// Display name of the user.
        /// This is obtained from the API descriptor using the FotoWeb API after successful authentication.
        /// It is used to demonstrate how to use the API with an OAuth access token.
        /// </summary>
        public string UserDisplayName { get; set; }

        #region "Widgets"

        /// <summary>
        /// URI of the FotoWeb selection widget.
        /// This is computed from the tenant URI and the access token (which must be passed to the widget).
        /// It is used to demonstrate how to use embeddable widgets in OAuth applications.
        /// </summary>
        public string SelectionWidgetUri
        {
            get
            {
                // API APPS ONLY
                // The access token can be used to make API calls directly.
                // Pass it in the FRAGMENT of the selection widget URI, so the selection widget can use it immediately.
                // Example: https://acme.fotoware.cloud/fotoweb/widgets/selection#access_token=ACCESS_TOKEN
                if (!ApplicationDetails.IsSelectionWidgetApp)
                {
                    var fragment = System.Web.HttpUtility.ParseQueryString(string.Empty);
                    fragment["access_token"] = AccessToken;
                    return new UriBuilder(ApplicationDetails.TenantUrl) { Path = "/fotoweb/widgets/selection", Fragment = fragment.ToString() }.Uri.ToString();
                }

                // CMS INTEGRATION APPS ONLY
                // The access token cannot be used to make API calls, because the application is not allowed to do so.
                // Instead, the FotoWeb server will receive the access token and use it to authorize use of the widget.
                // Pass it in the QUERY of the selection widget URI, so the FotoWeb server can validate it.
                // Example: https://acme.fotoware.cloud/fotoweb/widgets/selection?access_token=ACCESS_TOKEN
                else
                {
                    var query = System.Web.HttpUtility.ParseQueryString(string.Empty);
                    query["access_token"] = AccessToken;
                    return new UriBuilder(ApplicationDetails.TenantUrl) { Path = "/fotoweb/widgets/selection", Query = query.ToString() }.Uri.ToString();
                }
            }
        }

        /// <summary>
        /// Get URI of the FotoWeb export widget
        /// This is computed from the tenant URI and the access token (which must be passed to the widget).
        /// Note that the widget expects an additional "i" query string parameter, which must be the asset URL of an asset to export.
        /// </summary>
        public string ExportWidgetUri
        {
            get
            {
                var query = System.Web.HttpUtility.ParseQueryString(string.Empty);
                query["access_token"] = AccessToken;
                return new UriBuilder(ApplicationDetails.TenantUrl) { Path = "/fotoweb/widgets/publish", Query = query.ToString() }.Uri.ToString();
            }
        }

        #endregion

        #region "State Management"

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="appDetails"></param>
        private State(ApplicationDetails appDetails)
        {
            Id = _GenerateRandomString(32);
            ApplicationDetails = appDetails;
            CodeVerifier = _GenerateRandomString(32);
        }

        /// <summary>
        /// Create a new state
        /// </summary>
        /// <returns>State object</returns>
        public static State Create(ApplicationDetails appDetails)
        {
            lock (_lock)
            {
                var state = new State(appDetails);

                _states[state.Id] = state;

                return state;
            }
        }

        /// <summary>
        /// Look up an existing state
        /// </summary>
        /// <param name="state">State ID</param>
        /// <returns></returns>
        public static State Get(string state)
        {
            lock( _lock )
            {
                try
                {
                    return _states[state];
                }
                catch( KeyNotFoundException )
                {
                    return null;
                }
            }
        }

        private static Object _lock = new Object();
        private static Dictionary<string, State> _states = new Dictionary<string, State>();

        /// <summary>
        /// Compute BASE64URL encoding of the given binary data
        /// </summary>
        /// <param name="bytes">Binary data</param>
        /// <returns>BASE64URL encoding of binary data</returns>
        private static string _EncodeBase64URL(byte[] bytes)
        {
            var customBase64 = HttpServerUtility.UrlTokenEncode(bytes);
            return customBase64.Substring(0, customBase64.Length - 1);  // UrlTokenEncode adds an additional number to the end
        }

        /// <summary>
        /// Generates a sequence of random bytes and encodes it as BASE64URL.
        /// This is needed for the state parameter and for the PKCE code verifier of the OAuth 2.0 protocol.
        /// RFC 7636 recommends generating a sequence of 32 random bytes and encoding it as BASE64URL.
        /// The same approach can be used for the state parameter.
        /// </summary>
        /// <param name="numBytes">Number of bytes</param>
        /// <returns>A random string of BASE64URL characters</returns>
        private static string _GenerateRandomString(int numBytes)
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                byte[] bytes = new byte[numBytes];
                rng.GetBytes(bytes);
                return _EncodeBase64URL(bytes);
            }
        }

        /// <summary>
        /// Computes the SHA256 hash value of the given code verifier string and encodes it as BASE64URL.
        /// The result is the code challenge to be sent with an OAuth 2.0 authorization request.
        /// </summary>
        /// <param name="codeVerifier">Code verifier</param>
        /// <returns>Code challenge</returns>
        private static string _GetPKCEHash(string codeVerifier)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                return _EncodeBase64URL(sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(codeVerifier)));
            }
        }
        #endregion
    }
}