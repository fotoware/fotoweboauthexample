﻿$(function () {
    function updateForm() {
        var hasDemoServer = (demoServerTenantURL !== '' && demoServerTenantURL !== null);
        if (hasDemoServer)
            $('#use-demo-server').show();
        else
            $('#use-demo-server').hide();

        // Show or hide form fields and instructions based on application type.
        var appType = $('#appType').val();
        if (appType === '0')
            var visibleFields = $('.fields-web');
        else if (appType === '1')
            var visibleFields = $('.fields-js');
        else if (appType === '2')
            var visibleFields = $('.fields-native');
        else if (appType === '3')
            var visibleFields = $('.fields-cms');
        else if (appType === '4')
            var visibleFields = $('.fields-cmsSinglePage');
        else if (appType === '5')
            var visibleFields = $('.fields-cmsClassic');

        // Show or hide form fields and instructions based on whether the demo server is used.
        var tenantUrl = $('#tenantURL').val();
        var isDemoServer = (hasDemoServer && tenantUrl === demoServerTenantURL);
        if (isDemoServer)
            visibleFields = visibleFields.not('.fields-demo-server-hidden');
        else
            visibleFields = visibleFields.not('.fields-demo-server-shown');

        // Show or hide warning about CSP
        if (window.location.protocol == "https:" && tenantUrl.substring(0, 5) == "http:")
            visibleFields = visibleFields.add('.fields-csp');
        else
            visibleFields = visibleFields.not('.fields-csp');

        // Show or hide warning that FotoWeb must be accessible on the internet
        if (window.location.hostname.includes(".fotoware.cloud"))
            visibleFields = visibleFields.add('.fields-internet');
        else
            visibleFields = visibleFields.not('.fields-internet');
        
        $('.all-fields').hide();
        visibleFields.show();
    }

    // Update the form when page is loaded
    updateForm();

    // Update the form when application type or tenant URL is changed
    $('#appType').change(updateForm);
    $('#tenantURL').on('input', updateForm);

    // The redirection link is meant for copying the URL only.
    // If the user clicks on it, show a popup with instructions.
    $('.redir-link').click(function () {
        alert("Please copy the URL of this link into the clipboard and paste it into the application registration form as a redirection endpoint URI");
        return false;
    })

    // Both the "Run Example" and "Permalink" buttons submit the form.
    // However, when clicking "Permalink", the 'Run' property is set to False, so the form is shown again.
    // The URL is updated to contain all the application details, so they can be copied and reused.
    $('#submit').click(function () {
        $('#run').val('True');
    })
    $('#permalink').click(function () {
        if ($('#clientSecret').val() !== '')
            alert("Note that by sharing the link to the application, you are also sharing the client secret!");
        $('#run').val('False');
    })

    // If the demo server is selected, reset the form.
    $('#use-demo-server').click(function () {
        $('#tenantURL').val(demoServerTenantURL);
        $('#clientID').val('');
        $('#clientSecret').val('');

        updateForm();
    })
});
