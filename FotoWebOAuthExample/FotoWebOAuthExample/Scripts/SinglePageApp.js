﻿// Example / Test OAuth 2.0 single-page JavaScript application.
// This code performs OAuth authorization using implicit grant.
// It is used for single page JavaScript applications using the FotoWeb API as well as CMS integrations using only the widgets.
// Most of this code can be copied into a real-world application.

// The global variable 'application' contains all the information about the application.
// It is initialized in Index.cshtml (in SinglePageApp) with information from the server.
// In a real-world single page application, all attributes may be hard-coded.

// The OAuth 2.0 access token
var accessToken = null;

// Create a random string for the state parameter
function createState()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_~.";
    for (var i = 0; i < 16; ++i)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

// Redirect to the OAuth 2.0 authorization endpoint.
// This function is only used if the user navigates directly to the URL of the single page application.
// It shows how to build an authorization URL directly from JavaScript.
function requestAuthorization()
{
    // If the application accepts parameters, it is possible to store them in session storage and retrieve them later.
    // OAuth authorization involves a few page reloads, so anything stored in JavaScript variables is lost.
    //
    // In this application, we need the FotoWeb tenant URL as a parameter.
    // Since this is a single-page JavaScript application, we cannot rely on the server to deliver the parameter.
    // (We pretend that there is no server.)
    //
    // Note that it is also possible to pass parameters through the "state" parameter,
    // but it will be sent and exposed to FotoWeb, which is unnecessary.
    //
    // Session storage is linked to the current browser tab and will be deleted when the tab is closed.
    // Other tabs or websites cannot access it.
    sessionStorage.setItem('application', JSON.stringify(application));

    // Create a random state parameter and store it in session storage.
    // The state is used to prevent cross-site request forgery (CSRF) and embedding into other apps (mash-ups)
    // by ensuring that the app only accepts access tokens it has itself requested.
    var state = createState();
    sessionStorage.setItem('state', state);

    // Navigate to OAuth authorization endpoint
    window.location = application.tenantURL + "/fotoweb/oauth2/authorize?" +
        "response_type=token&" +
        "client_id=" + application.clientID + "&" +
        "redirect_uri=" + encodeURIComponent(application.redirectURI) + "&" +
        "state=" + encodeURIComponent(state);
}

// Extract the OAuth 2.0 access token from the URL
function getAccessToken(state)
{
    if (!state) return null;

    var params = window.location.hash.substring(1).split('&');
    var token = null;
    var stateValid = false;
    var errorCode = null;
    var errorDescription = "";
    var errorUri = "";
    for (var i = 0; i < params.length; ++i)
    {
        var pair = params[i].split('=');
        var key = decodeURIComponent(pair[0]);
        var value = decodeURIComponent(pair[1]);
        if (key === 'access_token')
        {
            token = value;
        }
        else if (key === 'state') {
            if (state === value)
                stateValid = true;
            else
            {
                showError("Response from server has invalid state.")
                return null;
            }
        }
        else if (key === "error") {
            errorCode = value;
        }
        else if (key === "error_description") {
            errorDescription = value;
        }
        else if (key === "error_uri") {
            errorUri = value;
        }
    }

    if (!stateValid)
    {
        showError("Response from server missing state parameter.")
        return null;
    }

    if (errorCode) {
        // FotoWeb has redirected back with an error.
        showError("Authorization Failed", errorCode, errorDescription);
        return null;
    }

    if (!token)
    {
        showError("Response from server missing access token parameter");
        return null;
    }

    return token;
}

// Show an authorization error
function showError(errorMessage, errorCode, errorDescription)
{
    $('#errorMessage').text(errorMessage);

    if (errorCode)
    {
        if (errorCode === "access_denied")
        {
            $('#errorDetails').text("Access was denied by the server or by the user. This usually means that the user does not have permission to use the application or applications in general, that the server does not have an API license or that the user denied consent to the application.");
        }
        else
        {
            $('#errorDetails').text("Authorization has failed or was denied by FotoWeb or by the user. FotoWeb has redirected the user agent to this error page with additional information about the error. The application may implement further error handling in the application endpoint. This error page is an example. It is designed for debugging and not particularly user-friendly. The most simple way to handle OAuth 2.0 errors is to redirect back to the home page of the application.");
        }
        $('#errorCode').text(errorCode);
        $('#errorDescription').text(errorDescription);
        $('#oauthError').show();
    }
    else
    {
        $('#oauthError').hide();
    }

    $('#loginView').show();
    $('#loginView').show();
    $('#errorView').show();
}

// Called when page is loaded
function init()
{
    // If redirected from server (OAuth response), get application details and state from session storage (set by requestAuthorization).
    var state;
    if (window.location.hash)
    {
        application = JSON.parse(sessionStorage.getItem('application'));
        state = sessionStorage.getItem('state');

        if (!application)
        {
            // User probably got here using the "back" button.
            // Application details are no longer available, so we just go back to the start page.
            window.location = "/";
            return;
        }
    }

    // Clean up session storage
    sessionStorage.removeItem('application');
    sessionStorage.removeItem('state');
    
    accessToken = getAccessToken(state);
    if (!accessToken)
    {
        // Login button starts OAuth authorization process
        $('#loginButton').click(requestAuthorization);
    }
    else
    {
        // We have an access token.
        // Show the main view.
        $('#loginView').hide();
        $('#errorView').hide();
        $('#mainView').show();

        // Set the selection widget URI which is used by Widgets.js
        if (!application.isSelectionWidgetApp)
            // Regular API applications pass the access token in the fragment
            application.selectionWidgetURI = application.tenantURL + "/fotoweb/widgets/selection#access_token=" + encodeURIComponent(accessToken);
        else
            // CMS integration applications pass the access token in the query string
            application.selectionWidgetURI = application.tenantURL + "/fotoweb/widgets/selection?access_token=" + encodeURIComponent(accessToken);

        // Set the export widget URI which is used by Widgets.js
        application.exportWidgetURI = application.tenantURL + "/fotoweb/widgets/publish?access_token=" + encodeURIComponent(accessToken);
    }
}

$(init);
