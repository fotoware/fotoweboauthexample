﻿// Interaction with embeddable FotoWeb widgets.
// The widget itself is shown by simply loading its URL in an <IFRAME> tag.
// When the user is done interacting with a widget, the widget sends a 'message' event to the embedding application.
// These events are handled here.
// The rest of this code is mostly about showing and hiding the widgets in modal dialogs.
// A real application may use different approaches to embed and present the widgets.

$(function ()
{
    // Hide loading indicator when widget has been loaded
    $('iframe').load(function ()
    {
        $('.loading').hide();
    });

    // If button is clicked, show selection widget
    $('#selectionWidgetButton').click(function ()
    {
        $('#selectionWidgetModal').on('shown.bs.modal', function ()
        {
            $('.loading').show();
            $(this).find('iframe').attr('src', application.selectionWidgetURI);
        });

        // Show modal dialog
        $('#selectionWidgetModal').modal({ show: true });
    });

    // Handle messages from widgets.
    // This is where data is received from the FotoWeb widget for later use.
    window.addEventListener('message', function (evt)
    {
        console.log("I DID get a message");

        // Ignore events that do not originate from a FotoWeb widget.
        // For events from widgets, the origin is the URL of the FotoWeb tenant.
        // The global variable 'application' contains all the information about the application.
        // It is initialized in Index.cshtml (in SinglePageApp) with information from the server.
        // In a real-world single page application, all attributes may be hard-coded.
        if (evt.origin !== application.tenantURL)
            return;

        if (evt.data.event === 'assetSelected')
        {
            // This message is sent by the selection widget when the user has selected an asset.
            // The event data contains the asset URL and asset information.
            // In this example, the asset is passed on to the export widget, which is typical for CMS integrations.
            var assetURL = evt.data.asset['href'];

            // Hide modal dialog
            $('.modal').modal('hide');

            // Load export widget
            $('#exportWidgetModal').on('shown.bs.modal', function ()
            {
                $('.loading').show();
                $(this).find('iframe').attr('src', application.exportWidgetURI + "&i=" + encodeURIComponent(assetURL));
            });

            // Show modal dialog
            $('#exportWidgetModal').modal({ show: true });
        }
        else if (evt.data.event === 'assetExported')
        {
            // This message is sent by the export widget when the user has published the asset.
            // The event data contains the exported image URLs.
            // In this exampple, the image is embedded into the main view of the application.
            // This is similar to what a CMS does (e.g., embedding the link to the image into a page or blog post)
            var imageURL = evt.data.export.export.image.normal;

            // Hide modal dialog
            $('.modal').modal('hide');

            // Get URL of exported image
            var exportedImageURL = imageURL;

            // Show exported image
            $('#exportedImage').find('img').attr('src', exportedImageURL);
            $('#exportedImage').show();
        }
        else if (evt.data.event === 'selectionWidgetCancel')
        {
            // Hide modal dialog
            $('.modal').modal('hide');
        }
    });
});
